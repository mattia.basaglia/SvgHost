#!/usr/bin/env bash
#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Description:
#
# This script is a helper to manage Django servers using python 3.
# It manages the python environment and the server daemon.
#

# =============================================================================
# Environment
# -----------------------------------------------------------------------------
# The variables defined here are documented by help.
# =============================================================================

[ "$VERBOSE" = 1 ] && set -x

: ${PYTHON:="python"}
: ${PYTHON_FLAGS:=""}
: ${ENV_NAME:="env_svghost"}

: ${DJANGO_HOST:="0.0.0.0"}
: ${DJANGO_PORT:="8080"}

: ${PIDFILE:="log/pid"}
: ${LOGFILE:="log/server.log"}

SITE_DIR="$(realpath "$(dirname "${BASH_SOURCES[0]}")" )"

# =============================================================================
# Common Functions
# =============================================================================

# Removes all spaces from stdin
function nospaces()
{
    tr -d '[ \t]'
}

# Ensures $1 is created as a directory if it doesn't exist
# It will fail if $1 exists as something else or the user doesn't have the right permissions
function ensure_dir()
{
    local dir="$(dirname "$1")"
    [ -d "$dir" ] || mkdir -p "$dir"
}

# Calls the correct python interpreter with the proper arguments
# More arguments are forwarded
function python_interpreter()
{
    "$PYTHON" $PYTHON_FLAGS "$@"
}

# Starts the server in the background, redirecting the output to $LOGFILE
# and storing the server process id in the $PIDFILE
# Returns non-zero if the server is already running
function start()
{
    ! running || return 1
    ensure_dir "$PIDFILE"
    ensure_dir "$LOGFILE"
    rm -f "$LOGFILE" # TODO Create backup
    nohup "$PYTHON" $PYTHON_FLAGS "$SITE_DIR/manage.py" runserver "$DJANGO_HOST:$DJANGO_PORT" &>"$LOGFILE" &
    echo $! >"$PIDFILE"
}

# Runs the server synchronously
# TODO avoid duplication of the command
function run()
{
    "$PYTHON" $PYTHON_FLAGS "$SITE_DIR/manage.py" runserver "$DJANGO_HOST:$DJANGO_PORT"
}

# Returns the server process id
# PRE: The $PIDFILE exists and contains the process id
function pid()
{
    cat "$PIDFILE"
}

# Stops the running server
# Returns non-zero if the server is not running
function stop()
{
    if running
    then
        local pid="$(pid)"
        local pgid="$(ps -p "$(pid)" -o pgid= | tr -d '[[:space:]]')"
        rm -f "$PIDFILE"
        [ "$pgid" ] && kill -TERM -"$pgid" || kill -TERM "$pid"
    else
        return 1
    fi
}

# Returns whether the server is running
function running()
{
    [ -f "$PIDFILE" ] && kill -0 "$(pid)" &>/dev/null
}

# Converts its arguments into a string, separated by the first argument
function implode()
{
    local IFS="$1"
    shift
    echo "$@"
}

# Text formatting, outputs the ansi sequence
# ^n = Color n
# ^* = Clear color
# b = bold
# i = italic
# u = underline
# upper case = remove the corresponding flag
# no arguments = clear all formatting
function tf()
{
    local flags=()
    for arg in "$@"
    do
        case "$arg" in
            ^\*)
                flags+=(39)
                ;;
            ^*)
                flags+=("3$(echo "$arg" | cut -c 2)")
                ;;
            b)
                flags+=(1)
                ;;
            B)
                flags+=(22)
                ;;
            i)
                flags+=(3)
                ;;
            I)
                flags+=(23)
                ;;
            u)
                flags+=(4)
                ;;
            U)
                flags+=(24)
                ;;
        esac
    done
    [ "$flags" ] || flags=(0)
    echo -e "\x1b[$(implode \; "${flags[@]}")m"
}

# Whether $1 has been declared as a function
function is_function()
{
    [ "$(type -t "$1")" = "function" ]
}

# =============================================================================
# Commands
# -----------------------------------------------------------------------------
# Here are defined the script commands:
#
# Functions with the name cmd_<command> will be extracted and <command>
# will be available as a new command for the script.
#
# To define a help message for <command>, output something from a function
# called help_cmd_<command>. The output will be prepended wis 8 spaces.
#
# To document arguments shown by the help synopsis, define a function called
# args_cmd_<command> which should output everythin in a single line and with
# proper formatting.
#
# If a command needs extra information that does not fit in a shost description,
# define a function called extra_cmd_<command>, this will only be seen
# when showing the help for that specific command, not for the general help
# =============================================================================

function cmd_help()
{
    if [ "$1" ] && is_function "cmd_$1"
    then
        local cmd="$1"
        shift
        echo "$(tf b)SYNOPSIS$(tf)"
        echo -n "    $0 $(tf b)$cmd$(tf)"
        is_function "args_cmd_$cmd" && echo -n " $("args_cmd_$cmd")"
        echo
        echo
        echo "$(tf b)DESCRIPTION$(tf)"
        if is_function "help_cmd_$cmd"
        then
            "help_cmd_$cmd" | while read line
            do
                echo "    $line"
            done
            echo
        fi
        echo
        if is_function "extra_cmd_$cmd"
        then
            echo "$(tf b)EXTRA INFORMATION$(tf)"
            "extra_cmd_$cmd" "$@"
            echo
        fi
        return
    fi

    echo "$(tf b)NAME$(tf)"
    echo "    $(basename "$0") - manage a server instance"
    echo
    echo "$(tf b)SYNOPSIS$(tf)"
    echo "    $0 $(tf u)command$(tf) [$(tf i)args$(tf)]..."
    echo
    echo "$(tf b)COMMANDS$(tf)"
    for cmd in $(declare -F | grep -Po "declare -f cmd_\K\S+")
    do
        echo -n "    $(tf b)$cmd$(tf)"
        is_function "args_cmd_$cmd" && echo -n " $("args_cmd_$cmd")"
        echo
        if is_function "help_cmd_$cmd"
        then
            "help_cmd_$cmd" | while read line
            do
                echo "        $line"
            done
            echo
        fi
    done
    echo
    echo "$(tf b)ENVIRONMENT$(tf)"
    cat <<ENV | column -tx -s '#'
    $(tf b)VERBOSE$(tf)# #If $(tf b)1$(tf), $0 will print each command it runs.
    $(tf b)PYTHON$(tf)#$PYTHON #Default python interpreter.
    $(tf b)PYTHON_FLAGS$(tf)#$PYTHON_FLAGS #Extra flags for the python interpreter.
    $(tf b)ENV_NAME$(tf)#$ENV_NAME #Virtual environment name.
    $(tf b)DJANGO_HOST$(tf)#$DJANGO_HOST #Host for $(tf b)runserver$(tf).
    $(tf b)DJANGO_PORT$(tf)#$DJANGO_PORT #Port number for $(tf b)runserver$(tf).
    $(tf b)PIDFILE$(tf)#$PIDFILE #File used to store the server process id.
    $(tf b)LOGFILE$(tf)#$LOGFILE #File used to store the server process id.
ENV
    echo
    echo "    Environment variables can be overwritten in a file called $(tf i)svghost.conf$(tf)."
    echo

}

function help_cmd_help()
{
    echo "Show a list of commands or the help for a specific command."
}

function args_cmd_help()
{
    echo "[$(tf u)command$(tf)]"
}

function cmd_start()
{
    if start && sleep 0.5 && running
    then
        echo "Server started"
    else
        echo "Could not start the server"
        return 1
    fi
}

function help_cmd_start()
{
    echo "Starts a server daemon."
}

function cmd_stop()
{
    if stop
    then
        echo "Server stopped"
    else
        echo "Server not running"
        return 1
    fi
}

function help_cmd_stop()
{
    echo "Stops a server daemon started using $(tf b)$0 start$(tf)."
}

function cmd_status()
{
    if running
    then
        local pid="$(pid)"
        local address="$(ps -p "$pid" -o args= | grep -Po "runserver\s+\K\S+")"
        echo "Server is running as process $pid on $address"
    else
        echo "Server is not running"
    fi
}

function help_cmd_status()
{
    echo "Show status information of a running server daemon."
}

function cmd_run()
{
    run
}

function help_cmd_run()
{
    echo "Starts the server in the foreground."
}

function cmd_uptime()
{
    ps -p "$(pid)" -o etime= | nospaces
}

function help_cmd_uptime()
{
    echo "Show the time a server daemon has been running for."
}

function cmd_restart()
{
    running && stop && echo "Server stopped"
    cmd_start
}

function help_cmd_restart()
{
    echo "Stops and restarts the server daemon."
}

function cmd_custom()
{
    python_interpreter "$SITE_DIR/manage.py" "$@"
}

function help_cmd_custom()
{
    echo "Forwards a command to $(tf b)manage.py$(tf)."
    echo "The advancege over using $(tf b)manage.py$(tf) directly is that"
    echo "this will ensure the correct environment is being used."
}

function args_cmd_custom()
{
    echo "$(tf u)manage-command$(tf) [$(tf i)args$(tf)]..."
}

function extra_cmd_custom()
{
    cmd_custom help "$@"
}

function cmd_rogue()
{
    local processes="$(ps ajx)"
    local rogue="$(echo "$processes" | ( running && grep -v "$(pid)" || cat - ) | grep "$SITE_DIR/manage.py")"
    if [ "$rogue" ]
    then
        echo "$processes" | head -n 1
        echo "$rogue"
    else
        echo "No rogue instances found"
    fi
}

function help_cmd_rogue()
{
    echo "Prints a list of potentially rogue server instances, that is"
    echo "server instances that appear to be using the same settings"
    echo "as the one started with $(tf b)$0 start$(tf) but which cannot"
    echo "be stopped with $(tf b)$0 start$(tf)."
}

function cmd_viewlog()
{
    less +F "$LOGFILE"
}

function help_cmd_viewlog()
{
    echo "Shows the log of the currently running server."
}

function cmd_init_env()
{
    virtualenv -p "$PYTHON" "$ENV_NAME"
    source "$ENV_NAME/bin/activate"
    pip install -r "$SITE_DIR/requirements.pip"
    if [ -f "requirements-extra.pip" ]
    then
        pip install -r "requirements-extra.pip"
    fi
}

function help_cmd_init_env()
{
    echo "Sets up the virtual environment."
    echo "To install extra packages, create a file called"
    echo "$(tf b)requirements-extra.pip$(tf)"
}

function cmd_init_db()
{
    cmd_custom makemigrations svghost
    cmd_custom migrate
}

function help_cmd_init_db()
{
    echo "Creates and applies migrations."
}

function cmd_url()
{
    echo "http://$(hostname -f):$DJANGO_PORT"
}

function help_cmd_url()
{
    echo "Shows the url for the server (based on the current hostname and port number)."
}

# =============================================================================
# Script body
# =============================================================================

# Load config for the sccript
if [ -f svghost.conf ]
then
    source svghost.conf
elif [ -f "$SITE_DIR/svghost.conf" ]
then
    source "$SITE_DIR/svghost.conf"
fi

# Load the python environment
if [ -f "$ENV_NAME/bin/activate" ]
then
    source "$ENV_NAME/bin/activate"
fi

# Select the python interpreter
if ! ( python_interpreter --version 2>&1 | grep -q "Python 3" )
then
    PYTHON=python3
    if ! python_interpreter --version &>/dev/null
    then
        echo "Cannot find a Python 3 installation" >&2
        exit 1
    fi
fi

# Parse the command line
func=cmd_"$1"
shift

if is_function "$func"
then
    "$func" "$@"
else
    cmd_help
fi
