#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib import admin
from django.contrib import auth

from . import models


admin.site.register(models.Profile)
admin.site.register(models.Image)
admin.site.register(models.ImageType)
admin.site.register(models.FileExtension)
admin.site.register(models.Directory)
admin.site.register(models.License)
admin.site.register(models.Comment)


class ProfileInline(admin.StackedInline):
    model = models.Profile
    can_delete = False

class UserAdmin(auth.admin.UserAdmin):
    inlines = (ProfileInline, )

admin.site.unregister(auth.models.User)
admin.site.register(auth.models.User, UserAdmin)
