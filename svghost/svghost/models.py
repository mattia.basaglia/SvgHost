#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import uuid
from django.db import models
from django.contrib.auth.models import User
from django.db.models import signals

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.OneToOneField("Image", on_delete=models.SET_NULL, null=True, blank=True)
    can_upload = models.ManyToManyField("ImageType", related_name="uploaders", blank=True)
    can_embed = models.ManyToManyField("ImageType", related_name="embedders", blank=True)

    def __str__(self):
        return "Profile for %s (%s)" % (self.user.email, self.user.username)

    @staticmethod
    def _create(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

signals.post_save.connect(Profile._create, sender=User)


class File(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    basename = models.CharField(max_length=128)
    creation_date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)
    hidden = models.BooleanField(default=False)

    class Meta:
        abstract = True

    @staticmethod
    def _upload_file(instance, filename):
        return 'images/{1}/{2}'.format(instance.owner.id, instance.uuid)


class Image(File):
    description = models.TextField()
    authors = models.ManyToManyField(User, related_name="all_images", blank=True)
    type = models.ForeignKey("ImageType", on_delete=models.PROTECT)
    license = models.ForeignKey("License", on_delete=models.PROTECT) # Many Many?
    modified_date = models.DateTimeField(auto_now=True)
    image = models.FileField(upload_to=File._upload_file)

    def upload_file(self):
        return File._upload_file(self, self.basename)


class ImageType(models.Model):
    name = models.CharField(max_length=32)
    mime_type = models.CharField(max_length=128)
    text = models.BooleanField()
    priority = models.IntegerField(default=0)
    extensions = models.ManyToManyField("FileExtension")


class FileExtension(models.Model):
    extension = models.CharField(max_length=16)


class Directory(File):
    parent = models.ForeignKey("Directory", on_delete=models.CASCADE, null=True)
    images = models.ManyToManyField("Image")


class License(models.Model):
    short_name = models.CharField(max_length=32)
    description = models.TextField()
    url = models.URLField(null=True, blank=True)


class Comment(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ManyToManyField(Image)
    text = models.TextField()
