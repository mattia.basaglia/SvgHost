#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import url

from .views import *

app_name="svghost"
urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^user/(?P<user_id>[0-9]+)/?$', user_by_id, name='user_by_id'),
    url(r'^u/(?P<user_name>[-A-Za-z0-9_]+)/?$', user_by_name, name='user_by_name'),
    url(r'^upload/?$', upload, name='upload'),
    url(r'^image/(?P<uuid>[-A-Fa-f0-9]+)/?$', image, name='image'),
]
