#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils.translation import ugettext as _
from . import models, forms

def home(request):
    return HttpResponse("Todo")

def _user_profile(request, user):
    context = {
        "profile_user": user,
    }
    return render(request, "svghost/profile.htm", context)

def user_by_id(request, user_id):
    return _user_profile(request, get_object_or_404(models.User, id=user_id))

def user_by_name(request, user_name):
    return _user_profile(request, get_object_or_404(models.User, username=user_name))

def upload(request):
    if request.method == "POST":
        form = forms.SingleImage(request.POST, request.FILES)
        if form.is_valid():
            new_file = models.Image(image = request.FILES['image'])
            new_file.save()
            return HttpResponseRedirect(reverse('svghost:image', uuid=new_file.uuid))
    else:
        form = forms.SingleImage()

    context = {
        "form": form,
    }
    return render(request, "svghost/upload.htm", context)

def image(request, uuid):
    return render(request, "svghost/image.htm")
