#
# Copyright (C) 2015 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import url, include
from django.contrib import admin, auth
import django.contrib.auth.views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', auth.views.login, {'template_name': 'login.htm'}, name='login'),
    url(r'^logout/$', auth.views.logout, name='logout'),
    url(r'^', include('svghost.svghost.urls')),
]
